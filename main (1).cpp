#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct Arg
{
	HANDLE sem_count_readers;
	HANDLE sem_flag_writers;
	HANDLE sem_readers;
	HANDLE sem_writers;
	HANDLE sem_print;
	HANDLE event_readers;
	int count_writer;
	int count_reader;
};

void writer(LPVOID func_arg)
{
	Arg *arg = (Arg*)func_arg;
	for (;;)
	{
		++(arg->count_writer);
		WaitForSingleObject(arg->sem_writers, INFINITE);
		--(arg->count_writer);
		if (arg->count_writer == 0)
			SetEvent(arg->event_readers);

		
		WaitForSingleObject(arg->sem_print, INFINITE);
		printf("----Writer came in\n");
		ReleaseSemaphore(arg->sem_print, 1, NULL);

		Sleep(rand() % 1000 + 1000);

		ReleaseSemaphore(arg->sem_writers, 1, NULL);
		//--(arg->count_writer);

		WaitForSingleObject(arg->sem_print, INFINITE);
		printf("----Writer came out\n");
		ReleaseSemaphore(arg->sem_print, 1, NULL);

		
		Sleep(rand() % 3000 + 1000);
	}
}

void reader(LPVOID func_arg)
{
	Arg *arg = (Arg*)func_arg;
	for (;;)
	{
		if (arg->count_writer > 0)
			WaitForSingleObject(arg->event_readers, INFINITE);

		WaitForSingleObject(arg->sem_count_readers, INFINITE);
		if (arg->count_reader == 0)
			WaitForSingleObject(arg->sem_writers, INFINITE);
		++(arg->count_reader);

		WaitForSingleObject(arg->sem_print, INFINITE);
		printf("Reader came in\t\t");
		printf("Count reader: %d\n", arg->count_reader);
		ReleaseSemaphore(arg->sem_print, 1, NULL);


		ReleaseSemaphore(arg->sem_count_readers, 1, NULL);

		Sleep(rand() % 5000 + 2000);

		WaitForSingleObject(arg->sem_count_readers, INFINITE);
		--(arg->count_reader);

		WaitForSingleObject(arg->sem_print, INFINITE);
		printf("Reader came out\t\t");
		printf("Count reader: %d\n", arg->count_reader);
		ReleaseSemaphore(arg->sem_print, 1, NULL);

		if (arg->count_reader == 0)
			ReleaseSemaphore(arg->sem_writers, 1, NULL);
		ReleaseSemaphore(arg->sem_count_readers, 1, NULL);

		//ReleaseSemaphore(arg->sem_readers, 1, NULL);

		Sleep(rand() % 500);
	}
}

void writers_and_readers(size_t number_of_writers, size_t number_of_readers)
{
	HANDLE *writer_threads = (HANDLE*)malloc(number_of_writers * sizeof(HANDLE));
	HANDLE *reader_threads = (HANDLE*)malloc(number_of_readers * sizeof(HANDLE));

	HANDLE sem_count_readers = CreateSemaphore(NULL, 1, 1, NULL);
	HANDLE sem_flag_writers = CreateSemaphore(NULL, 1, 1, NULL);
	HANDLE sem_readers = CreateSemaphore(NULL, number_of_readers, number_of_readers, NULL);
	HANDLE sem_writers = CreateSemaphore(NULL, 1, 1, NULL);
	HANDLE sem_print = CreateSemaphore(NULL, 1, 1, NULL);
	HANDLE event_readers = CreateEvent(NULL, true, false, NULL);

	Arg *arg = (Arg*)malloc(sizeof(Arg));
	arg->sem_count_readers = sem_count_readers;
	arg->sem_flag_writers = sem_flag_writers;
	arg->sem_readers = sem_readers;
	arg->sem_writers = sem_writers;
	arg->sem_print = sem_print;
	arg->event_readers = event_readers;
	arg->count_writer = 0;
	arg->count_reader = 0;

	for (size_t i = 0; i < number_of_readers; ++i)
		reader_threads[i] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)reader, arg, 0, NULL);

	for (size_t i = 0; i < number_of_writers; ++i)
		writer_threads[i] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)writer, arg, 0, NULL);

	WaitForMultipleObjects(number_of_writers, writer_threads, true, INFINITE);

	free(writer_threads);
	free(reader_threads);
}

int main()
{
	size_t number_of_writers, number_of_readers;
	printf("Number of writers = ");
	scanf_s("%u", &number_of_writers);
	printf("Number of readers = ");
	scanf_s("%u", &number_of_readers);
	writers_and_readers(number_of_writers, number_of_readers);
	return 0;
}