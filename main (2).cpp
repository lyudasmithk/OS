#include <windows.h>
#include <stdio.h>
#include <list>

using namespace std;

struct Client
{
	HANDLE client_event;
	size_t client_number;
};

struct Arg
{
	HANDLE *mutex;
	HANDLE *barber_event;
	list<Client*> *queue;
	int max_size_of_queue;
	bool *sleep;
	size_t client_number;
	HANDLE *print_mutex;
};

void barber(LPVOID func_arg)
{
	Arg *arg = (Arg*)func_arg;
	HANDLE *barber_event = arg->barber_event;
	list<Client*> *queue = arg->queue;
	bool *sleep = arg->sleep;
	HANDLE *mutex = arg->mutex;
	for (;;)
	{
		WaitForSingleObject(*mutex, INFINITE);
		if (queue->size() == 0)
		{
			*sleep = true;
			ReleaseMutex(*mutex);
			WaitForSingleObject(*barber_event, INFINITE);
		}
		else
		{
			size_t client_number = queue->front()->client_number;
			SetEvent(queue->front()->client_event);
			queue->pop_front();
			ReleaseMutex(*mutex);

			WaitForSingleObject(arg->print_mutex, INFINITE);
			printf("barber - client %d start\n", client_number);
			ReleaseMutex(arg->print_mutex);
			
			Sleep(rand() % 3000 + 2000);

			WaitForSingleObject(arg->print_mutex, INFINITE);
			printf("barber - client %d end\n", client_number);
			ReleaseMutex(arg->print_mutex);
		}
	}
}

void client(LPVOID func_arg)
{
	Arg *arg = (Arg*)func_arg;
	HANDLE *barber_event = arg->barber_event;
	list<Client*> *queue = arg->queue;
	int max_size_of_queue = arg->max_size_of_queue;
	bool *sleep = arg->sleep;
	HANDLE *mutex = arg->mutex;

	Client client;
	client.client_event = CreateEvent(NULL, false, false, NULL);
	client.client_number = arg->client_number;

	WaitForSingleObject(arg->print_mutex, INFINITE);
	printf("client %d come\n", client.client_number);
	ReleaseMutex(arg->print_mutex);
	
	if (queue->size() < max_size_of_queue)
	{
		WaitForSingleObject(*mutex, INFINITE);
		queue->push_back(&client);
		if (*sleep)
		{
			SetEvent(*barber_event);
			*sleep = false;
		}
		ReleaseMutex(*mutex);
		WaitForSingleObject(client.client_event, INFINITE);

		WaitForSingleObject(arg->print_mutex, INFINITE);
		printf("client %d leave\n", client.client_number);
		ReleaseMutex(arg->print_mutex);
	}
	else
	{
		WaitForSingleObject(arg->print_mutex, INFINITE);
		printf("client %d leave\n", client.client_number);
		ReleaseMutex(arg->print_mutex);
	}

	CloseHandle(client.client_event);
}

void sleeping_barber(int max_size_of_queue)
{
	list<Client*> queue;

	HANDLE barber_event = CreateEvent(NULL, false, false, NULL);
	HANDLE mutex = CreateMutex(NULL, false, NULL);
	HANDLE print_mutex = CreateMutex(NULL, false, NULL);
	Arg *arg = (Arg*)malloc(sizeof(Arg));
	arg->barber_event = &barber_event;
	arg->max_size_of_queue = max_size_of_queue;
	arg->queue = &queue;
	arg->mutex = &mutex;
	arg->print_mutex = &print_mutex;
	arg->sleep = (bool*)malloc(sizeof(bool));
	HANDLE barber_thread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)barber, arg, 0, NULL);

	for (int i = 0;; ++i)
	{
		arg->client_number = i;
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)client, arg, 0, NULL);
		Sleep(rand() % 3000 + 1000);
	}
}

int main()
{
	int max_size_of_queue;
	printf("Size of queue:");
	scanf_s("%d", &max_size_of_queue);
	sleeping_barber(max_size_of_queue);
	return 0;
}