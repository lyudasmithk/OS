#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <psapi.h>
#include <time.h>
#define MAX_NAME 256

double CalculateCPULoad(ULONG64 processTime, ULONG64 totalTime)
{
	double ret = (double)processTime / totalTime;

	return ret * 100;
}

ULONG64 FileTimeToInt64(const FILETIME &ft)
{ 
	return (((ULONG64)(ft.dwHighDateTime)) << 32) | ((ULONG64)ft.dwLowDateTime);
}

double GetCPULoad(HANDLE hProcess)
{
	FILETIME creationTime, exitTime, processKernelTime, processUserTime, idleTime, totalKernelTime, totalUserTime;
	if (GetProcessTimes(hProcess, &creationTime, &exitTime, &processKernelTime, &processUserTime) &&
		GetSystemTimes(&idleTime, &totalKernelTime, &totalUserTime))
	{
		return CalculateCPULoad(FileTimeToInt64(processKernelTime) + FileTimeToInt64(processUserTime),
			FileTimeToInt64(totalKernelTime) + FileTimeToInt64(totalUserTime));
	}
	else
		return 0;
}

void PrintProcessInfo(DWORD processID)
{
	TCHAR szProcessName[MAX_PATH] = TEXT("<unknown>");
	TCHAR userName[MAX_NAME];
	TCHAR userName1[MAX_NAME];
	HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
			FALSE, processID);

	HMODULE hMod;
	DWORD cbNeeded;
	if (EnumProcessModules(hProcess, &hMod, sizeof(hMod), &cbNeeded))
	{
		GetModuleBaseName(hProcess, hMod, szProcessName, sizeof(szProcessName));
	}

	HANDLE hToken;
	DWORD dwSize = 0;
	SID_NAME_USE SidType;
	OpenProcessToken(hProcess, TOKEN_QUERY, &hToken);
	GetTokenInformation(hToken, TokenUser, NULL, dwSize, &dwSize);
	PTOKEN_USER pUserInfo;
	pUserInfo = (PTOKEN_USER)GlobalAlloc(GPTR, dwSize);
	GetTokenInformation(hToken, TokenUser, pUserInfo, dwSize, &dwSize);
	dwSize = MAX_NAME;
	if (!LookupAccountSid(NULL, pUserInfo->User.Sid, userName, &dwSize, userName1, &dwSize, &SidType))
	{ 
		wcscpy_s(userName, TEXT("<unknown>"));
	}

	DWORD exitCode;
	TCHAR statusProcess[20];
	if (GetExitCodeProcess(hProcess, &exitCode))
	{
		if (exitCode == STILL_ACTIVE)
			wcscpy_s(statusProcess, TEXT("Active"));
		else
			_itow_s(exitCode, statusProcess, 10);
	}
	else
	{
		wcscpy_s(statusProcess, TEXT("----"));
	}

	PROCESS_MEMORY_COUNTERS pmc;
	TCHAR workingSetMemory[20] = TEXT("----");
	if (GetProcessMemoryInfo(hProcess, &pmc, sizeof(pmc)))
	{
		_itow_s(pmc.WorkingSetSize / 1024, workingSetMemory, 10);
	}

	double CpuLoad = GetCPULoad(hProcess);

	wprintf(TEXT("%-10.10s %5u %-10.10s %-7s %9s %4.1f\n"), szProcessName, processID, userName, statusProcess, workingSetMemory, CpuLoad);

	CloseHandle(hProcess);
}

void ProcessInfo()
{
	DWORD aProcesses[1024], cbNeeded, cProcesses;
	unsigned int i;

	if (!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded))
	{
		return;
	}

	cProcesses = cbNeeded / sizeof(DWORD);

	wprintf(TEXT("Name:      PID:    User:   Status:    Memory:   CPU:\n"));

	for (i = 0; i < cProcesses; i++)
	{
		if (aProcesses[i] != 0)
		{
			PrintProcessInfo(aProcesses[i]);
		}
	}
}

int main(void)
{
	MEMORYSTATUSEX memInfo;
	memInfo.dwLength = sizeof(MEMORYSTATUSEX);
	GlobalMemoryStatusEx(&memInfo);

	wprintf(TEXT("Total physcal memory: %llu MB\n"), memInfo.ullTotalPhys / 1024 / 1024);
	wprintf(TEXT("Available physcal memory: %llu MB\n"), memInfo.ullAvailPhys / 1024 / 1024);
	wprintf(TEXT("Total size of page file: %llu MB\n"), memInfo.ullTotalPageFile / 1024 / 1024);

	SYSTEM_INFO sysInfo;
	GetSystemInfo(&sysInfo);

	wprintf(TEXT("Size of page memory: %lu KB\n"), sysInfo.dwPageSize / 1024);

	ProcessInfo();

	system("pause");
	return 0;
}