#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <unistd.h>

using namespace std;

class List
{
    struct Node
    {
        Node *next;
    };
    Node *begin;
    Node *end;

public:
    List() : begin(nullptr), end(nullptr) {}
    void add_node()
    {
        if (begin == nullptr)
        {
            begin = new Node;
            begin->next = nullptr;
            end = begin;
        }
        else
        {
            Node *new_node = new Node;
            new_node->next = nullptr;
            end->next = new_node;
            end = new_node;
        }
    }
    void delete_node()
    {
        if (begin != end)
        {
            Node *temp = begin;
            begin = begin->next;
            delete temp;
        }
        else
        {
            delete begin;
            begin = end = nullptr;
        }
    }
};

void consumer(List& list, size_t &size_of_list, size_t max_size_of_buffer,
    mutex &consumer_mutex, mutex &size_mutex, mutex &common_mutex, mutex &print_mutex)
{
    bool is_lock_common_mutex = false;
    size_t temp;
    for (;;)
    {
        consumer_mutex.lock();
        if (size_of_list > 0)
        {
            if (size_of_list == 1)
            {
                common_mutex.lock();
                is_lock_common_mutex = true;
            }
            list.delete_node();

            size_mutex.lock();
            temp = --size_of_list;
            size_mutex.unlock();

            if (is_lock_common_mutex)
            {
                common_mutex.unlock();
                is_lock_common_mutex = false;
            }

            print_mutex.lock();
            cout << "size of list = " << size_of_list << endl;
            print_mutex.unlock();
        }
        consumer_mutex.unlock();
        usleep(rand() % 3000000 + 1000000);
    }
}

void producer(List& list, size_t &size_of_list, size_t max_size_of_buffer,
    mutex &producer_mutex, mutex &size_mutex, mutex &common_mutex, mutex &print_mutex)
{
    bool is_lock_common_mutex = false;
    size_t temp;
    for (;;)
    {
        producer_mutex.lock();
        if (size_of_list < max_size_of_buffer)
        {
            if (size_of_list == 1)
            {
                common_mutex.lock();
                is_lock_common_mutex = true;
            }
            list.add_node();

            size_mutex.lock();
            temp = ++size_of_list;
            size_mutex.unlock();

            if (is_lock_common_mutex)
            {
                common_mutex.unlock();
                is_lock_common_mutex = false;
            }

            print_mutex.lock();
            cout << "size of list = " << temp << endl;
            print_mutex.unlock();
        }
        producer_mutex.unlock();
        usleep(rand() % 30000000 + 1000000);
    }
}

void producers_and_consumers(size_t max_size_of_buffer,
    size_t number_of_producers, size_t number_of_consumers)
{
    List list;
    size_t size_of_list = 0;
    mutex producer_mutex, consumer_mutex, size_mutex, common_mutex, print_mutex;
    vector<thread> producers_threads(number_of_producers);
    vector<thread> consumers_threads(number_of_consumers);

    for (size_t i = 0; i < number_of_producers; ++i)
        producers_threads[i] = thread(producer, list, ref(size_of_list), max_size_of_buffer,
            ref(producer_mutex), ref(size_mutex), ref(common_mutex), ref(print_mutex));

    for (size_t i = 0; i < number_of_consumers; ++i)
        consumers_threads[i] = thread(consumer, list, ref(size_of_list), max_size_of_buffer,
            ref(consumer_mutex), ref(size_mutex), ref(common_mutex), ref(print_mutex));



    for (size_t i = 0; i < number_of_producers; ++i)
        producers_threads[i].join();
    for (size_t i = 0; i < number_of_consumers; ++i)
        consumers_threads[i].join();
}

int main()
{
    srand(time(NULL));
    size_t number_of_producers, number_of_consumers, max_size_of_buffer;
    cout << "number of producers = ";
    cin >> number_of_producers;
    cout << "number of consumers = ";
    cin >> number_of_consumers;
    cout << "size of buffer = ";
    cin >> max_size_of_buffer;
    producers_and_consumers(max_size_of_buffer, number_of_producers, number_of_consumers);
}
